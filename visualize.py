import sqlite3
import os
import json

db_path = os.environ.get('SCAN_DB_PATH', os.path.dirname(os.path.abspath(__file__)) + os.path.sep + 'data.sqlite')
output_file = os.environ.get('OUTPUT_FILE', os.path.dirname(os.path.abspath(__file__)) + os.path.sep + 'graph.html')

# globals
con: sqlite3.Connection


def init_db() -> None:
    global con
    con = sqlite3.connect(db_path)


def terminate_db() -> None:
    global con
    if con:
        con.close()


def generate_file() -> None:
    global con
    cur = con.cursor()
    nodes = cur.execute('SELECT * FROM access_points').fetchall()
    node_list = []
    for node in nodes:
        color = '#e55252'
        if node[3] == 'none':
            color = '#90ed90'
        elif node[3] == 'wep':
            color = '#efa44f'

        node_list.append({
            "id": node[0],
            "label": node[1],
            "title": "Frequency: %s, Security: %s" % (node[2], node[3]),
            "color": color
        })

    edges = cur.execute('SELECT * FROM proximity').fetchall()
    edge_list = []
    for edge in edges:
        edge_list.append({
            "from": edge[0],
            "to": edge[1],
            "color": '#75c9e5',
        })

    options = {
        "edges": {
            "physics": False,
            "smooth": False,
        },
        "physics": {
            "solver": 'hierarchicalRepulsion',
            "stabilization": True,
        },
        "layout": {
            "improvedLayout": False,
        }
    }

    print('''
<html>
<head>
    <script type="text/javascript" src="https://unpkg.com/vis-network/standalone/umd/vis-network.min.js"></script>
</head>
<body>
    <div id="graph"></div>
    <script>
    // create an array with nodes
    var nodes = new vis.DataSet(%s);

    // create an array with edges
    var edges = new vis.DataSet(%s);

    // provide the data in the vis format
    var data = {
        nodes: nodes,
        edges: edges
    };

    // create a network
    var container = document.getElementById('graph');

    var options = %s;

    // initialize your network!
    var network = new vis.Network(container, data, options);
    </script>
</body>
</html>
    ''' % (json.dumps(node_list), json.dumps(edge_list), json.dumps(options)), file=open(output_file, 'w'))


if __name__ == '__main__':
    # first, lets initialize the db connection, table creation and all
    init_db()

    # do the actual hard work
    generate_file()

    # finalize stuffs
    terminate_db()
