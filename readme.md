# Python Wi-Fi Scanner

## Description

A simple Python-based Wi-Fi scanner, utilizing linux's `iwlist` utility

## Installation

1. Clone this repo
2. Install the required libraries via pip from [`requirements.txt`](requirements.txt)
3. Run the main script [`main.py`](main.py)

## Configuration

You can configure the scan script via the following environment variables:

| Variable            | Description | Default |
| ---                 | ---         | ---     |
| SCAN_DB_PATH        | Path to the sqlite file | `<script_dir>/data.sqlite` |
| SCAN_INTERFACE      | Which interface to use for scanning | `wlan0` |
| SCAN_SAVE_FREQUENCY | How often so save the database (in minutes). Set to 0 to save after every scan, set to -1 to only save on exit | 5 |

## Visualization

There is a [`visualize.py`](visualize.py) script which generates a static html file with
a visualization based on [vis-network](https://github.com/visjs/vis-network).

This is a work-in-progress though, and struggles when displaying a lot of access points.

The visualizer also has the following environment variables for configuration:

| Variable            | Description | Default |
| ---                 | ---         | ---     |
| SCAN_DB_PATH | Path to the sqlite file | `<script_dir>/data.sqlite` |
| OUTPUT_FILE | Where to output the finished html file | `<script_dir>/graph.html` |