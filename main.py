import sqlite3
import os
import sys
import signal
from time import time
from typing import List
from contextlib import contextmanager

import wifi.scan
from wifi import Cell

# The path to the database file, defaults to <current_path>/data.sqlite
# TODO: improve path handling
db_path = os.environ.get('SCAN_DB_PATH', os.path.dirname(os.path.abspath(__file__)) + os.path.sep + 'data.sqlite')
# The interface which to scan with, defaults to wlan0
interface = os.environ.get('SCAN_INTERFACE', 'wlan0')
# How often to save the database (in minutes), defaults to 5
# Set to 0 to save after every scan, set to -1 to only save on errors/exit
scan_frequency = int(os.environ.get('SCAN_SAVE_FREQUENCY', 5))

# Type hints
Cells = List[Cell]

# global database connections
# this one is the actual database on disk
disk_db: sqlite3.Connection
# and this one is a local in-memory copy of it
mem_db: sqlite3.Connection


def init_db() -> None:
    global disk_db, mem_db
    disk_db = sqlite3.connect(db_path)
    mem_db = sqlite3.connect(':memory:')
    cur = disk_db.cursor()
    # first we create the tables in the on-disk database if they don't exist already
    # TODO: improve data types (frequency to float/double, length of security & name)
    cur.execute('''CREATE TABLE IF NOT EXISTS access_points(
        mac_address VARCHAR(17),
        name VARCHAR(128),
        frequency VARCHAR(32),
        security VARCHAR(32),
        PRIMARY KEY (mac_address)
    ) WITHOUT ROWID''')
    cur.execute('''CREATE TABLE IF NOT EXISTS proximity(
        mac_address VARCHAR(17),
        neighbor_mac VARCHAR(17),
        PRIMARY KEY (mac_address, neighbor_mac)
    ) WITHOUT ROWID''')
    # and now we copy the on-disk database to memory
    disk_db.backup(mem_db)


def write_db() -> None:
    global disk_db, mem_db
    if disk_db and mem_db:
        print('Saving database to disk...')
        mem_db.backup(disk_db)


def terminate_db() -> None:
    global disk_db, mem_db
    if disk_db and mem_db:
        write_db()
        mem_db.close()
        disk_db.close()


def get_networks() -> Cells:
    try:
        with time_limit(10):
            networks: Cells = list(Cell.all(interface))
    except KeyboardInterrupt:
        # TODO: could probably be handled better
        raise KeyboardInterrupt()
    except TimeoutError:
        raise TimeoutError('Network scan timed out')
    except:
        raise wifi.scan.InterfaceError()

    # the sorting here happens mainly for ease of use (+ it prevents duplicates in store_proximity)
    def sort_by_address(cell: Cell):
        return cell.address

    networks.sort(key=sort_by_address)
    return networks


def store_networks(networks: Cells) -> int:
    global mem_db
    cur = mem_db.cursor()
    values = []
    for net in networks:
        encryption = 'none'
        if net.encrypted:
            encryption = net.encryption_type
        values.append((net.address, net.ssid, net.frequency, encryption))
    res = cur.executemany('INSERT OR REPLACE INTO access_points VALUES (?, ?, ?, ?)', values)
    mem_db.commit()
    return res.rowcount


def store_proximity(networks: Cells) -> int:
    global mem_db
    cur = mem_db.cursor()
    values = []
    for i in range(len(networks) - 1):
        for j in range(i + 1, len(networks)):
            val = (networks[i].address, networks[j].address)
            existing = cur.execute('SELECT * FROM proximity WHERE mac_address = ? AND neighbor_mac = ?', val).fetchall()
            if len(existing) < 1:
                values.append(val)

    res = cur.executemany('INSERT INTO proximity VALUES (?, ?)', values)
    mem_db.commit()
    return res.rowcount


# Helper function that throws a TimeoutError when execution time runs out
# to use: `with time_limit(10):`
@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutError("Timed out!")
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)


if __name__ == '__main__':
    exit_code = 0
    # first, lets initialize the db connection, table creation and all
    init_db()

    # scan_frequency is in minutes, so we get the second amount for calculating the passed time
    scan_frequency_secs = scan_frequency * 60

    err_count = 0
    next_save = time() + scan_frequency_secs
    while True:
        if err_count >= 5:
            print('Too many errors, exiting...', file=sys.stderr)
            exit_code = 1
            break

        try:
            # next, we get the list of networks
            nets = get_networks()
            print('Found %i networks' % len(nets))

            # and here comes the juicy part
            store_networks(nets)
            store_proximity(nets)

            if scan_frequency_secs >= 0 and time() >= next_save:
                next_save = time() + scan_frequency_secs
                write_db()

            # and if everything worked we can reset the consecutive error count again
            err_count = 0
        except KeyboardInterrupt:
            # TODO: instead of just listening to the KeyboardInterrupt, we should also have a SIGTERM handler
            break
        except sqlite3.Error:
            # Shouldn't happen, but as long as it doesn't repeat it should (hopefully) be fine
            print('Sqlite error', file=sys.stderr)
            err_count += 1
        except wifi.scan.InterfaceError:
            # Shouldn't happen, but we'll retry just in case it fixes itself
            print('Wifi interface error', file=sys.stderr)
            err_count += 1
        except TimeoutError:
            print('Wifi scan timeout', file=sys.stderr)
            err_count += 1
        except:
            # For now it's not an instant exit, but it could be depending on the type of error
            print('Unknown error', file=sys.stderr)
            err_count += 1

    # finalize stuffs
    terminate_db()

    exit(exit_code)
